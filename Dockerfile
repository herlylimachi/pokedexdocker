FROM mongo

COPY initpokedex.json /initpokedex.json
CMD mongoimport --host mongopokedex --db Pokedex --collection PokemonCollection --type json --file /initpokedex.json --jsonArray